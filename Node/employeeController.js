const express=require('express');
var router=express.Router();
const ObjectId=require('mongoose').Types.ObjectId;

var {Employee} = require('./employee');

router.get('/', (req,res)=>
{
    Employee.find((err,docx)=>
    {
if(!err)
{res.send(docx)}
else
{console.log('error in getting records'+ JSON.stringify(err,undefined,2));
}});
});


router.post('/',(req,res)=>
{
    var emp=new Employee(
    {
        name:req.body.name,
        position:req.body.position,
        office:req.body.office,
        salary:req.body.salary,

    });
    emp.save((err,docx)=>{
        if(!err)
            {res.send(docx)}
        else
        {console.log('error in getting records'+ JSON.stringify(err,undefined,2));
            }});
});

router.get('/:id',(req,res) => {
    if(!ObjectId.isValid(req.params.id))
    return res.status(400).send(`No Record against this id :${req.params.id}`);

    
Employee.findById(req.params.id,(err,docx)=>
    {
if(!err)
{res.send(docx)}
else
{console.log('error in getting records'+ JSON.stringify(err,undefined,2));
}});



});


router.put('/:id',(req,res)=>
{
    if(!ObjectId.isValid(req.params.id))
    return res.status(400).send(`No Record against this id :${req.params.id}`);
    var emp=
        {
            name:req.body.name,
            position:req.body.position,
            office:req.body.office,
            office:req.body.salary,
        };
        Employee.findByIdAndUpdate(req.params.id,{$set:emp},{new:true},(err,docx)=>
        {
            if(!err)
{res.send(docx)}
else
{console.log('error updating'+ JSON.stringify(err,undefined,2));}
});

});



router.delete('/:id',(req,res)=>
{
    if(!ObjectId.isValid(req.params.id))
    return res.status(400).send(`No Record against this id :${req.params.id}`);


    Employee.findByIdAndRemove(req.params.id,(err,docx)=>
    {
if(!err)
{res.send(docx)}
else
{console.log('error in getting records'+ JSON.stringify(err,undefined,2));
}});
});

module.exports=router;