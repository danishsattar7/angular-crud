const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/CRUDS', (err) => {
    if (!err) {
        console.log('Connection Succesful');
    } else {
        console.log('Error ' + JSON.stringify(err, undefined, 2));
    }
});

module.exports = mongoose;