const bodyParser = require('body-parser');
const express = require('express');
const { mongoose } = require('./db');
const cors = require('cors');
var employeeController = require('./employeeController');

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}



var app = express();

app.use(allowCrossDomain);
app.use(bodyParser.json());
app.use(cors({ origin: 'http://localhost:4200' }));
app.listen(3000, () => { console.log('listning'); })

app.use('/employees', employeeController);