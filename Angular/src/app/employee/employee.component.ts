import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { EmployeeService } from '../shared/employee.service';
import { NgForm } from '@angular/forms';
import { Employee } from '../shared/employee.model';
import { fromEventPattern } from 'rxjs';

// import { ToastsManager } from 'ng2-toastr/ng2-toastr';
var M:any;

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  // constructor(private employeeService: EmployeeService,public toastr: ToastsManager, vcr: ViewContainerRef)
  constructor(private employeeService: EmployeeService) { 

    // this.toastr.setRootViewContainerRef(vcr);
  }
  
  ngOnInit() {
    this.resetForm();
    this.refereshEmployeeList();
  }
  refereshEmployeeList() {
this.employeeService.getEmployeeList().subscribe((res)=>
{
  this.employeeService.employees=res as Employee[];
});
}
  
  resetForm(form?: NgForm) {
    if (form) {
    form.reset();
    }

    this.employeeService.selectedEmployee = {
      _id: '',
      name: '',
      position: '',
      office: '',
      salary: ''
    };
  }

  onSubmit(form: NgForm) {

    if(form.value._id == ""){
this.employeeService.postEmployee(form.value).subscribe((res) => {
  this.resetForm(form);
  // this.toastr.success('Employee Record Inserted!', 'Success!');
  // M.toast({html: 'I am a toast!'});
  alert('Record Inserted');
  this.refereshEmployeeList();
});
  }
  else
  {
    this.employeeService.putEmployee(form.value).subscribe((res) => {
      this.resetForm(form);
      // this.toastr.success('Employee Record Inserted!', 'Success!');
      // M.toast({html: 'I am a toast!'});
      alert('Record Updated');
      this.refereshEmployeeList();
    });
  }
}


  onEdit(emp :Employee)
  {
    this.employeeService.selectedEmployee=emp;
  }

  onDelete(_id:string,form:NgForm)
  {
    if(confirm("Are You Sure To Delete This Record?")==true)
    {
      this.employeeService.deleteEmployee(_id).subscribe((res)=>
      {
        this.refereshEmployeeList();
        this.resetForm();
        alert('Record Deleted');
      
    });
  }
}
}