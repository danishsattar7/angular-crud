const express = require('express');


const ctrlUser = require('../controllers/user.controller');

const jwtHelper = require('../config/jwtHelper');

const router=express.Router();

router.post('/register',ctrlUser.register);
router.post('/authenticate', ctrlUser.authenticate);
router.get('/userProfile',jwtHelper.verifyJwtToken, ctrlUser.userProfile);

module.exports = router;