const mongoose=require('mongoose');
const bcrypt=require('bcryptjs');
var uniqueValidator = require('mongoose-unique-validator');
const jwt = require('jsonwebtoken');

var userSchema = new mongoose.Schema(
    {
    fullName:{type:String,
    required:"name cant be empty",
},
    email:{type:String,
        required:"email cant be empty",
    unique:true,},
    password:{type:String,
    required:"password cant be empty",
    minlength:[4,'Atleast needs 4 characters']},
    saltSecret:{type:String}    
    },{ versionKey: false });


userSchema.pre('save',function(next)
{
    bcrypt.genSalt(10,(err,salt)=>
    {
        bcrypt.hash(this.password,salt,(err,hash)=>
        {
            this.password=hash;
            this.saltSecret=salt;
            next();
        });
    });
});

userSchema.path('email').validate((val)=>
{
emailRegex =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
return emailRegex.test(val);
},'invalid-email');

userSchema.plugin(uniqueValidator);

// Methods
userSchema.methods.verifyPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.methods.generateJwt = function () {
    return jwt.sign({ _id: this._id},
        process.env.JWT_SECRET,
    {
        expiresIn: process.env.JWT_EXP
    });
}



mongoose.model('User', userSchema);

