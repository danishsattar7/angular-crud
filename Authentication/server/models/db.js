const mongoose=require('mongoose');
require('../models/user.model.js');


mongoose.connect(process.env.MONGODB_URI,(err)=>{
    if(!err)
    {
        console.log("Connection Successful");
    }
    else
    {
        console.log("Connection Failed :" + JSON.stringify(err,undefined,2)); 
    }
});