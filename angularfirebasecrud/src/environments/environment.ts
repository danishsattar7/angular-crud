// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig:{
    apiKey: "AIzaSyAyqdEbMTM5enGfsY7itr1TO2GPTQUJ1IQ",
    authDomain: "fir-crudapp-f0b4b.firebaseapp.com",
    databaseURL: "https://fir-crudapp-f0b4b.firebaseio.com",
    projectId: "fir-crudapp-f0b4b",
    storageBucket: "",
    messagingSenderId: "733419037011",
    appId: "1:733419037011:web:180bc3144d211895"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
